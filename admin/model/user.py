import json
from datetime import datetime

from mongoengine import *

class Base:
    created_by = StringField(default="system")
    created_at = DateTimeField(default=lambda: datetime.now)
    modified_by = StringField(default="system")
    modified_at = DateTimeField(default=lambda: datetime.now)


class User(Base, Document):
    user_name = StringField()
    email = StringField()
    first_name = StringField()
    last_name = StringField()
